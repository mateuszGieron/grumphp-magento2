
#!/bin/bash

./vendor/bin/phpcs --config-set installed_paths ../../magento/magento-coding-standard/,../../magento/marketplace-eqp/;

echo "export PATH=\"\$PATH:$PWD/vendor/bin\"" >> ~/.bashrc;
. ~/.bashrc;

if which zsh >/dev/null; then
    echo "export PATH=\"\$PATH:$PWD/vendor/bin\"" >> ~/.zshrc;
    zsh ~/.zshrc;
fi