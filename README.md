Grumphp
================

Usage
-----

**Install**

`git clone git@bitbucket.org:mateuszGieron/grumphp-magento2.git`

`cd grumphp-magento2`

`composer install`

**Verify if Magento2 and MEQP1 standards added**

`vendor/bin/phpcs -i`

**Init target git repository**

Init GrumPHP configuration for Magento 2 projects
-

`grumphp git:init --config='[path-to-grumphp-magento2]/grumphp-magento2.yml'`

**Run code analyze manually**

`grumphp run --config='[path-to-grumphp-magento2]/grumphp-magento2.yml'`

Init GrumPHP configuration for Magento 1 projects
-

`grumphp git:init --config='[path-to-grumphp-magento2]/grumphp-magento1.yml'`

**Run code analyze manually**

`grumphp run --config='[path-to-grumphp-magento2]/grumphp-magento1.yml'`
